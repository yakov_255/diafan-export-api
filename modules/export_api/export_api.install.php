<?php
/**
 * Установка модуля
 *
 * @package    DIAFAN.CMS
 * @author     diafan.ru
 * @version    6.0
 * @license    http://www.diafan.ru/license.html
 * @copyright  Copyright (c) 2003-2018 OOO «Диафан» (http://www.diafan.ru/)
 */

if (!defined('DIAFAN')) {
    $path = __FILE__;
    while (!file_exists($path . '/includes/404.php')) {
        $parent = dirname($path);
        if ($parent == $path)
            exit;
        $path = $parent;
    }
    include $path . '/includes/404.php';
}


class Export_api_install extends Install
{
    /**
     * @var string название
     */
    public $title = "API выгрузки";

    /**
     * @var array таблицы в базе данных
     */
    public $tables = array(
        array(
            "name" => "export_api",
            "comment" => "Настройки выгрузки",
            "fields" => array(
                array(
                    "name" => "id",
                    "type" => "SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT",
                    "comment" => "идентификатор",
                ),
                array(
                    "name" => "name",
                    "type" => "VARCHAR(50) NOT NULL DEFAULT ''",
                    "comment" => "Название",
                    "multilang" => true,
                ),
                array(
                    "name" => "act",
                    "type" => "ENUM('0', '1') NOT NULL DEFAULT '0'",
                    "comment" => "показывать на сайте: 0 - нет, 1 - да",
                    "multilang" => true,
                ),
                array(
                    "name" => "api_key",
                    "type" => "VARCHAR(50) NOT NULL DEFAULT ''",
                    "comment" => "Ключ API",
                ),
                array(
                    "name" => "provider",
                    "type" => "VARCHAR(50) NOT NULL DEFAULT ''",
                    "comment" => "Провайдер api",
                ),
                array(
                    "name" => "params",
                    "type" => "TEXT NOT NULL DEFAULT ''",
                    "comment" => "Сериализованные настройки провайдера",
                ),
                array(
                    "name" => "use_utf8",
                    "type" => "ENUM('0', '1') NOT NULL DEFAULT '0'",
                    "comment" => "кодировка: 0 - windows 1251, 1 - utf8",
                ),
                array(
                    "name" => "trash",
                    "type" => "ENUM('0', '1') NOT NULL DEFAULT '0'",
                    "comment" => "запись удалена в корзину: 0 - нет, 1 - да",
                ),
            ),
            "keys" => array(
                "PRIMARY KEY (id)",
            ),
        ),
    );


    /**
     * @var array записи в таблице {modules}
     */
    public $modules = array(
        array(
            "name" => "export_api",
            "site" => true, // Может отображать код на сайте (работает show_block)
            "admin" => true,
        ),
    );
    /**
     * @var array страницы сайта
     */
    public $site = array(
        array(
            "sort" => 99,
            "name" => array('api', 'api'),
            "act" => true,
            "module_name" => "export_api",
            "rewrite" => "api",
        ),
    );


    /**
     * @var array меню административной части
     */
    public $admin = array(
        array(
            "name" => "API выгрузки",
            "rewrite" => "export_api",
            "group_id" => 4,
            "sort" => 14,
            "act" => true,
            "children" => array(
                array(
                    "name" => "API выгрузки",
                    "rewrite" => "export_api",
                    "sort" => 1,
                    "act" => true,
                )
            )
        ),
    );


}
