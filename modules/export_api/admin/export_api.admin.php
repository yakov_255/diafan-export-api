<?php
/**
 * Редактирование методов оплаты
 *
 * @package    DIAFAN.CMS
 * @author     diafan.ru
 * @version    6.0
 * @license    http://www.diafan.ru/license.html
 * @copyright  Copyright (c) 2003-2018 OOO «Диафан» (http://www.diafan.ru/)
 */

if (!defined('DIAFAN')) {
    $path = __FILE__;
    while (!file_exists($path . '/includes/404.php')) {
        $parent = dirname($path);
        if ($parent == $path)
            exit;
        $path = $parent;
    }
    include $path . '/includes/404.php';
}

/**
 * Shop_admin_cityselector
 */
class Export_api_admin extends Frame_admin
{
    /**
     * @var string таблица в базе данных
     */
    public $table = 'export_api';

    /**
     * @var array поля в базе данных для редактирования
     */
    public $variables = array(
        'main' => array(
            'name' => array(
                'type' => 'text',
                'name' => 'Название',
                'multilang' => true,
            ),
            'api_key' => array(
                'type' => 'text',
                'name' => 'Ключ',
            ),
            'act' => array(
                'type' => 'checkbox',
                'name' => 'Включить',
                'default' => true,
                'multilang' => true,
            ),
            'use_utf8' => array(
                'type' => 'select',
                'name' => 'Кодировка файла ответа',
                'select' => array(
                    "0" => 'Windows 1251',
                    "1" => 'UTF-8',
                ),
                'default' => 1,
            ),
            'provider' => array(
                'type' => 'function',
                'name' => 'Провайдер API',
            ),
        ),
    );

    /**
     * @var array поля в списка элементов
     */
    public $variables_list = array(
        'checkbox' => '',
        'name' => array(
            'name' => 'Название'
        ),
        'provider' => array(
            'name' => 'Провайдер API',
            'type' => 'text',
            'sql' => true,
        ),
        'api_key' => array(
            'name' => 'Ключ API',
            'type' => 'text',
            'sql' => true,
        ),
        'actions' => array(
            'trash' => true,
        ),
    );

    public function init()
    {
        if ($this->diafan->is_action("edit")) {

            $rows = $this->get_rows();

            foreach ($rows as $provider) {
                $provider_name = $provider['name'];
                $title = $provider['config']['name'];

                $table = array();
                foreach ($provider['config']['params'] as $key => $row) {
                    $table[$provider_name . '_' . $key] = $row;
                }

                $table = array_merge(
                    array('hr1' => array(
                        'type' => 'title',
                        'name' => 'Настройки ' . $title,
                    ),)
                    , $table);
                $this->variables['provider_' . $provider_name] = $table;
            }

        }

        if ($this->diafan->is_action("save")) {

        }

        return parent::init();
    }

    /**
     * Выводит ссылку на добавление
     * @return void
     */
    public function show_add()
    {
        $this->diafan->addnew_init('Добавить');
    }


    /**
     * Вывод списка тэгов
     * @return void
     */
    public function show()
    {
        $this->diafan->list_row();
    }

    public function list_variable_api_key($row, $var)
    {
        $link = BASE_PATH . "api?key=" . $row['api_key'];

        return '<a href="' . $link . '">' . $link . '</a>';
    }

    public function validate_variable_api_key()
    {
        if (empty($_POST["api_key"])) {
            $this->diafan->set_error("api_key", "Поле Ключ api обязательно для заполнения.");
            return;
        }

        if (strlen($_POST["api_key"]) < 3 || strlen($_POST["api_key"]) > 40) {
            $this->diafan->set_error("api_key", "Допустимая длина 3-40");
        }

        if ((!preg_match('/^\w+$/', $_POST["api_key"]))) {
            $this->diafan->set_error("api_key", "Допустимые символы a-z, 0-9");
        }
    }

    /**
     * Редактирование поля "Служба доставки"
     * @return void
     */
    public function edit_variable_provider()
    {
        $rows = $this->get_rows();

        echo '		
		<div class="unit">
			<div class="infofield">
				' . $this->diafan->variable_name() . '
			</div>
			<select name="provider"><option value="">-</option>';
        foreach ($rows as $row) {
            echo '<option value="' . $row["name"] . '"' . ($this->diafan->value == $row["name"] ? ' selected' : '') . '>' . $this->diafan->_($row["config"]["name"]) . '</option>';

            if (!$this->diafan->is_new) {
                $js_values[$row['name']] = unserialize(DB::query_result("SELECT params FROM {" . $this->diafan->table . "} WHERE id=%d LIMIT 1", $this->diafan->id));

                foreach ($row['config']['params'] as $param_name => $param) {
                    if (isset($js_values[$row['name']][$param_name])) {
                        $js_values[$row['name']][$param_name] = array('value' => $js_values[$row['name']][$param_name], 'type' => $param['type']);
                    }

                }

            }
        }
        echo '</select>
		</div>';
    }

    /**
     * Сохранение поля "Служба доставки"
     * @return void
     */
    public function save_variable_provider()
    {
        // Очитска кэша при сохранении настроек
        $this->diafan->_cache->delete('', 'export_api');

        if (empty($_POST['provider'])) {
            $this->diafan->set_query("provider='%s'");
            $this->diafan->set_value('');
            return;
        }

        $provider = $this->diafan->filter($_POST, "string", "provider");
        if (!Custom::exists('modules/export_api/api_providers/' . $provider . '/provider.' . $provider . '.admin.php')) {
            $this->diafan->set_query("provider='%s'");
            $this->diafan->set_value('');
            return;
        }
        Custom::inc('modules/export_api/api_providers/' . $provider . '/provider.' . $provider . '.admin.php');
        $config_class = 'Provider_' . $provider . '_admin';
        $class = new $config_class($this->diafan);

        $values = array();
        foreach ($class->config["params"] as $key => $name) {
            if (!empty($name["type"]) && $name["type"] == 'function') {
                if (is_callable(array(&$class, "save_variable_" . $key))) {
                    $value = call_user_func_array(array(&$class, "save_variable_" . $key), array());
                    if ($value) {
                        $values[$key] = $value;
                    }
                    continue;
                }
            }
            if (!empty($_POST[$provider . '_' . $key])) {
                $values[$key] = $this->diafan->filter($_POST, 'string', $provider . '_' . $key);
            }
        }
        $this->diafan->set_query("provider='%s'");
        $this->diafan->set_value($provider);

        $this->diafan->set_query("params='%s'");
        $this->diafan->set_value(serialize($values));
    }

    /**
     * Получает список всех платежных систем
     *
     * @return array
     */
    private function get_rows()
    {
        $rows = array();
        $rs = Custom::read_dir("modules/export_api/api_providers");
        foreach ($rs as $row) {
            $path = 'modules/export_api/api_providers/' . $row . '/provider.' . $row . '.admin.php';
            if (Custom::exists($path)) {
                Custom::inc($path);
                $config_class = 'Provider_' . $row . '_admin';
                $class = new $config_class($this->diafan);
                $rows[] = array("name" => $row, "config" => $class->config);
            }
        }
        return $rows;
    }


    /**
     * Разворачивание параметров
     * @return mixed
     */
    public function get_values()
    {
        $values = DB::query_fetch_array("SELECT * FROM {" . $this->diafan->table . "} WHERE id=%d"
            . ($this->diafan->variable_list('actions', 'trash') ? " AND trash='0'" : '') . " LIMIT 1",
            $this->diafan->id
        );

        if (!empty($values['params'])) {
            $params = unserialize($values['params']);
            if (is_array($params)) {
                foreach ($params as $key => $row) {
                    $values[$values['provider'] . '_' . $key] = $row;
                }
                unset($values['params']);
            }

        }

        return $values;
    }

}
