<?php
/**
 * Редактирование способов доставки
 *
 * @package    DIAFAN.CMS
 * @author     diafan.ru
 * @version    6.0
 * @license    http://www.diafan.ru/license.html
 * @copyright  Copyright (c) 2003-2018 OOO «Диафан» (http://www.diafan.ru/)
 */

if (!defined('DIAFAN')) {
    $path = __FILE__;
    while (!file_exists($path . '/includes/404.php')) {
        $parent = dirname($path);
        if ($parent == $path)
            exit;
        $path = $parent;
    }
    include $path . '/includes/404.php';
}

class Provider_diafan_yandex_admin extends Frame_admin
{
    public $countryList;

    public $name = "Яндекс YML Диафан";

    public $config = Array(
        'name' => 'Яндекс YML Диафан',
        'params' => array(
            'nameshop' => array(
                'type' => 'text',
                'name' => 'Короткое название магазина',
                'help' => 'Название магазина для системы «Яндекс Маркет». Не должно содержать более 20 символов. Нельзя использовать слова, не имеющие отношения к наименованию магазина («лучший», «дешевый»), указывать номер телефона и т. п. Название магазина, должно совпадать с фактическим названием магазина, которое публикуется на сайте).',
            ),
            'currencyyandex' => array(
                'type' => 'select',
                'name' => 'Валюта',
                'help' => 'Валюта для системы «Яндекс Маркет».',
                'select' => array(
                    'RUR' => 'RUR',
                    'USD' => 'USD',
                    'EUR' => 'EUR',
                    'UAH' => 'UAH',
                    'BYN' => 'BYN',
                    'KZT' => 'KZT',
                ),
            ),
            'bid' => array(
                'type' => 'numtext',
                'name' => 'Основная ставка',
                'help' => 'Смотрите [инструкцию «Яндекс Маркет»](http://partner.market.yandex.ru/legal/tt/).',
            ),
            'cbid' => array(
                'type' => 'numtext',
                'name' => 'Ставка для карточек',
                'help' => 'Смотрите [инструкцию «Яндекс Маркет»](http://partner.market.yandex.ru/legal/tt/).',
            ),
            "cat_list" => array(
                'name' => 'Список id категорий через запятую',
                'type' => 'text',
                'help' => 'Например: 1,2,55. Если заполнено, будут выгружены все категории'
            ),
            'show_yandex_category' => array(
                'type' => 'checkbox',
                'name' => 'Использовать фильтр "Выгружать в Яндекс Маркет" в карточке категории',
                'help' => 'Если галочка не стоит, галочка "Выгружать на Яндекс" в карточке категории игнорируется',
            ),
            'show_yandex_element' => array(
                'type' => 'checkbox',
                'name' => 'Использовать фильтр "Выгружать в Яндекс Маркет" в карточке Товара',
                'help' => 'Если галочка не стоит, галочка "Выгружать на Яндекс" в карточке Товараы игнорируется',
            ),
            'show_yandex_param' => array(
                'type' => 'checkbox',
                'name' => 'Использовать фильтр "Выгружать в Яндекс Маркет" в карточке Характеристики',
                'help' => 'Если галочка не стоит, галочка "Выгружать на Яндекс" в карточке Характеристики игнорируется',
            ),
        ),

    );

}
