<?php

if (!defined('DIAFAN')) {
    $path = __FILE__;
    while (!file_exists($path . '/includes/404.php')) {
        $parent = dirname($path);
        if ($parent == $path)
            exit;
        $path = $parent;
    }
    include $path . '/includes/404.php';
}

require_once __DIR__ . "/export_api_interface.php";

/**
 *
 */
class Export_api extends Controller
{
    public function init()
    {
        try {
            try {

                $row = DB::query_fetch_array("SELECT provider, params, use_utf8 FROM {export_api} WHERE trash = '0' AND api_key = '%s' ", $_GET['key']);
                if (!$row) {
                    header("HTTP/1.0 404 Not Found");
                    exit();
                }

                // Кэш 24 часа
                $cache_meta = array(
                    "key" => $_GET['key'],
                    date('d-m-Y-H', time()),
                    $row
                );

                if (!MOD_DEVELOPER && !$result = $this->diafan->_cache->get($cache_meta, "export_api")) {

                    $result = $this->get($row);

                    $this->diafan->_cache->save($result, $cache_meta, "export_api");
                }

                header('Content-type: application/xml');
                // TODO: set filename
                header('Content-Disposition: inline; filename="myfile.txt"');

                echo $result;

                exit();
            } catch (Exception $e) {
                Dev::exception($e);
            }
        } catch (Exception $e) {
            header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
        }
        exit();
    }


    /**
     * Подключает способ доставки
     *
     * @return string
     */
    public function get($row)
    {

        include_once(Custom::path('modules/export_api/api_providers/' . $row["provider"] . '/provider.' . $row["provider"] . '.model.php'));
        $name_class_model = 'Provider_' . $row["provider"] . '_model';
        $model = new $name_class_model($this->diafan);
        if (!is_subclass_of($model, 'export_api_interface')) {
            throw new Exception($name_class_model . ' должен реализовать интерфейс export_api_interface.');
        }

        $row["params"] = unserialize($row["params"]);

        $row["params"]["use_utf8"] = $row['use_utf8'] == '1';

        $result = $model->get_rows($row["params"]);

        if ($row['use_utf8'] == '0') {
            Custom::inc('plugins/encoding.php');
            $result = utf::to_windows1251($result);
        }

        return $result;
    }


}
