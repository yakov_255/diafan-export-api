<?php
/**
 * @author yakov_255
 * @e-mail  yakov_255@mail.ru
 */

interface export_api_interface
{
    public function get_rows($params);
}
